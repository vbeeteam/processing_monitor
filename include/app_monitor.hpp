/**
******************************************************************************
* @file           : app_monitor.hpp
* @brief          : header file of application monitor
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef APP_MONITOR_H
#define APP_MONITOR_H

#include "config.hpp"

class ProcessInfo
{
private:
public:
    ProcessInfo()
    {
    }
    ~ProcessInfo()
    {
    }
    // attributes
    int pid;
    std::string name;
    float cpu;
    float ram;
    std::vector<int> ports_listen;
    bool is_running = false;
};

class AppMonitor
{
private:
    std::map<std::string, ProcessInfo> p_monitor;
    ConfigInfo m_app_info;
    bool is_report_error = false;

    int monitor_task(void);
    int report_task(void);
    bool report_api(std::string &api, std::string &token, std::string &data);
public:
    AppMonitor(ConfigInfo app_info){
        this->m_app_info = app_info;
        this->p_monitor.clear();
    }
    ~AppMonitor(){

    }
    int run(void);
};



#endif