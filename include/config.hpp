/**
******************************************************************************
* @file           : config.hpp
* @brief          : header file of configuration
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef CONFIG_H
#define CONFIG_H

#include <vector>
#include <string>
#include <map>


class ConfigInfo{
private:
    
public:
    ConfigInfo(){
        this->process_name.clear();
        this->report_period = 30; // seconds
    }
    ~ConfigInfo(){

    }

    // attributes
    std::string application;
    std::string report_api;
    std::string api_token;
    std::string disk_path = "/";
    std::vector<std::string> process_name;
    int report_period;
    std::string ipAddress;
};


class ConfigHandler{
private:
    std::map<std::string, ConfigInfo> configs;
    std::string config_file;
    
    bool loadConfig(void);

public:
    ConfigHandler(std::string m_file)
    {
        this->config_file = m_file;
        this->loadConfig();
    }
    ~ConfigHandler()
    {
    }
    bool getConfig(std::map<std::string, ConfigInfo> &m_config)
    {
        m_config = this->configs;
        return true;
    }
};

#endif

