/**
******************************************************************************
* @file           : utility.cpp
* @brief          : Source file of utility function
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#include "utility/utility.hpp"
#include <sys/time.h>
#include <stdio.h>

#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <time.h>
#include <curl/curl.h>

#include <sys/time.h>
#include <time.h>
#include <sys/stat.h>
#include <fstream>
#include <sys/stat.h>
#include <iostream>
#include <cstdarg>
#include <string>
#include <fstream>
#include <memory>
#include <cstdio>
#include <ifaddrs.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <algorithm>
#include <sys/statvfs.h>

using namespace CommonEngine;

static inline unsigned long fast_strtoul(const char *c)
{
    int x = 0;

    // move pointer to first number
    for (; *c < 48 || *c > 57;)
        c++;

    // we are on a number, so convert it
    for (; *c > 47 && *c < 58;)
    {
        // shifting more efficient than multiplication
        x = (x << 1) + (x << 3) + *c - 48;

        c++;
    }

    return x;
}

static inline float mem(int *fd, const char **valp, char *mem_buff)
{
    lseek(*fd, 0, SEEK_SET);
    read(*fd, mem_buff, 256);
    float get_mem = 0;
    get_mem = ((((fast_strtoul(*valp) - fast_strtoul(*(valp + 1))) - (fast_strtoul(*(valp + 2)) + fast_strtoul(*(valp + 3)))) / 1024) / (fast_strtoul(*valp) / 1024.0)) * 100.0;

    return get_mem;
}

inline size_t fast_strlen(const char *str)
{
    int i; // Cannot return 'i' if inside for loop
    for (i = 0; str[i] != '\0'; ++i)
        ;

    return i;
}

static inline void mem_init(const char *meminfo[], const char buf[])
{
    const char *var[] = {
        "MemTotal:",
        "MemFree:",
        "Buffers:",
        "Cached:",
        0};

    for (size_t i = 0; var[i];)
    {
        // TODO: work on making own faster strstr func.
        meminfo[i] = strstr(buf, var[i]) + fast_strlen(var[i]);

        ++i;
    }
}

CpuMonitor::CpuMonitor()
{
}

CpuMonitor::~CpuMonitor()
{
}

void CpuMonitor::ReadStatsCPU(std::vector<CPUData> &entries)
{
    std::ifstream fileStat("/proc/stat");

    std::string line;

    const std::string STR_CPU("cpu");
    const std::size_t LEN_STR_CPU = STR_CPU.size();
    const std::string STR_TOT("tot");

    while (std::getline(fileStat, line))
    {
        // cpu stats line found
        if (!line.compare(0, LEN_STR_CPU, STR_CPU))
        {
            std::istringstream ss(line);

            // store entry
            entries.emplace_back(CPUData());
            CPUData &entry = entries.back();

            // read cpu label
            ss >> entry.cpu;

            // remove "cpu" from the label when it's a processor number
            if (entry.cpu.size() > LEN_STR_CPU)
                entry.cpu.erase(0, LEN_STR_CPU);
            // replace "cpu" with "tot" when it's total values
            else
                entry.cpu = STR_TOT;

            // read times
            for (int i = 0; i < NUM_CPU_STATES; ++i)
                ss >> entry.times[i];
        }
    }
}

size_t CpuMonitor::GetIdleTime(const CPUData &e)
{
    return e.times[S_IDLE] +
           e.times[S_IOWAIT];
}

size_t CpuMonitor::GetActiveTime(const CPUData &e)
{
    return e.times[S_USER] +
           e.times[S_NICE] +
           e.times[S_SYSTEM] +
           e.times[S_IRQ] +
           e.times[S_SOFTIRQ] +
           e.times[S_STEAL] +
           e.times[S_GUEST] +
           e.times[S_GUEST_NICE];
}

void CpuMonitor::PrintStats(const std::vector<CPUData> &entries1, const std::vector<CPUData> &entries2)
{
    const size_t NUM_ENTRIES = entries1.size();

    for (size_t i = 0; i < NUM_ENTRIES; ++i)
    {
        const CPUData &e1 = entries1[i];
        const CPUData &e2 = entries2[i];

        std::cout.width(3);
        std::cout << e1.cpu << "] ";

        const float ACTIVE_TIME = static_cast<float>(GetActiveTime(e2) - GetActiveTime(e1));
        const float IDLE_TIME = static_cast<float>(GetIdleTime(e2) - GetIdleTime(e1));
        const float TOTAL_TIME = ACTIVE_TIME + IDLE_TIME;

        std::cout << "active: ";
        std::cout.setf(std::ios::fixed, std::ios::floatfield);
        std::cout.width(6);
        std::cout.precision(2);
        std::cout << (100.f * ACTIVE_TIME / TOTAL_TIME) << "%";

        std::cout << " - idle: ";
        std::cout.setf(std::ios::fixed, std::ios::floatfield);
        std::cout.width(6);
        std::cout.precision(2);
        std::cout << (100.f * IDLE_TIME / TOTAL_TIME) << "%" << std::endl;
    }
}

uint32_t UtilityFunc::getTimestamp(void)
{
    struct timeval tv;
    unsigned long get_time = 0;
    gettimeofday(&tv, NULL);

    get_time = tv.tv_sec;
    return get_time;
}

uint32_t UtilityFunc::getTimeMs(void)
{
    struct timeval tv;
    uint32_t get_time = 0;
    gettimeofday(&tv, NULL);

    get_time = tv.tv_usec;
    return get_time / 1000;
}

uint8_t UtilityFunc::splitString(std::string data, std::string key_split, std::string *data_split)
{
    uint16_t pos = 0;
    uint16_t i = 0;

    while ((pos = data.find(key_split)) != std::string::npos)
    {
        data_split[i] = data.substr(0, pos);
        data.erase(0, pos + 1);
        i++;
        if (i > 9)
        {
            return 9;
        }
    }
    return i;
}

bool UtilityFunc::checkFile(std::string path)
{
    struct stat buffer;
    return (stat(path.c_str(), &buffer) == 0);
}

std::string UtilityFunc::fileName(std::string path)
{
    size_t found = path.find_last_of("/\\");
    std::string name = path.substr(found + 1); // check that is OK
    return name;
}

bool UtilityFunc::creatFile(std::string path)
{
    std::ofstream outfile(path, std::fstream::app);
    outfile.close();
    return true;
}

uint16_t UtilityFunc::getCurrentAsMinuteInDay(void)
{
    time_t rawTime;
    struct tm *timeInfo;
    time(&rawTime);
    timeInfo = localtime(&rawTime);
    uint8_t hour = timeInfo->tm_hour;
    uint8_t minute = timeInfo->tm_min;
    uint32_t currentMinute = hour * 60 + minute;
    return currentMinute;
}

bool UtilityFunc::parseUrl(const std::string &url, fields_url_info &urlInfo)
{
    bool result = false;

    CURLU *h;
    CURLUcode uc;
    char *host;
    char *path;
    char *port;
    char *scheme;
    h = curl_url(); /* get a handle to work with */

    /* parse a full URL */
    uc = curl_url_set(h, CURLUPART_URL, url.c_str(), 0);
    if (!uc)
    {
        uc = curl_url_get(h, CURLUPART_HOST, &host, 0);
        if (!uc)
        {
            result = true;
            // printf("Host name: %s\n", host);
            urlInfo.host = std::string(host);
            curl_free(host);
        }
        uc = curl_url_get(h, CURLUPART_PATH, &path, 0);
        if (!uc)
        {
            // printf("Path: %s\n", path);
            urlInfo.path = std::string(path);
            curl_free(path);
        }
        uc = curl_url_get(h, CURLUPART_PORT, &port, 0);
        if (!uc)
        {
            // printf("Port: %s\n", port);
            std::string port_cvt = std::string(host);
            urlInfo.port = std::stoi(port_cvt);
            curl_free(port);
        }
        uc = curl_url_get(h, CURLUPART_SCHEME, &scheme, 0);
        if (!uc)
        {
            // printf("scheme: %s\n", scheme);
            urlInfo.scheme = std::string(scheme);
            curl_free(scheme);
        }
    }
    curl_url_cleanup(h);

    return result;
}

std::string UtilityFunc::exeCommand(const std::string &cmd)
{
    std::shared_ptr<FILE> pipe(popen(cmd.c_str(), "r"), pclose);
    if (!pipe)
        return "ERROR";
    char buffer[128];
    std::string result = "";
    while (!feof(pipe.get()))
    {
        if (fgets(buffer, 128, pipe.get()) != NULL)
            result += buffer;
    }
    return result;
}

bool UtilityFunc::getPIDByProcess(const std::string &p_name, int &pid)
{
    std::string cmd = "pidof -s " + p_name;
    std::string output = UtilityFunc::exeCommand(cmd);
    if (output.empty() == false)
    {
        pid = std::stoi(output);
        return true;
    }

    return false;
}

uint16_t UtilityFunc::getCpuUsage(void)
{
    std::string cmd = "top -b -d 1 -n3|grep Cpu | awk 'NR == 3 { print $2 }'";
    std::string get_out = UtilityFunc::exeCommand(cmd.c_str());
    if (!get_out.empty())
    {
        return (std::stof(get_out));
    }
    return 0;
    // std::vector<CPUData> entries1;
    // std::vector<CPUData> entries2;

    // CpuMonitor cpu;

    // // snapshot 1
    // cpu.ReadStatsCPU(entries1);

    // // 100ms pause
    // usleep(100000);

    // // snapshot 2
    // cpu.ReadStatsCPU(entries2);

    // // print output
    // const CPUData &e1 = entries1[0];
    // const CPUData &e2 = entries2[0];
    // const float ACTIVE_TIME = static_cast<float>(cpu.GetActiveTime(e2) - cpu.GetActiveTime(e1));
    // const float IDLE_TIME = static_cast<float>(cpu.GetIdleTime(e2) - cpu.GetIdleTime(e1));
    // const float TOTAL_TIME = ACTIVE_TIME + IDLE_TIME;
    // uint16_t value = 100.f * ACTIVE_TIME / TOTAL_TIME;
    // //std::cout << "get cpu usage: " << std::to_string(value) << "%" << std::endl;
    // return value;
}

uint16_t UtilityFunc::getRamUsage(void)
{
    int mem_file = open("/proc/meminfo", 0);
    const char *meminfo[] = {0, 0, 0, 0, 0};
    char mem_buff[256]; // Just take what's needed
    read(mem_file, mem_buff, 256);
    mem_init(meminfo, mem_buff);

    uint16_t get_mem = mem(&mem_file, meminfo, mem_buff);

    close(mem_file);

    return get_mem;
}

float UtilityFunc::getCpuUsageByPID(int &pid)
{
    float value = 0;
    std::string cmd = "ps -p " + std::to_string(pid) + " -o %cpu | tail -1 | awk '{print $1}'";
    std::string output = UtilityFunc::exeCommand(cmd);
    if (output.empty() == false)
    {
        value = std::stof(output);
    }

    return value;
}

float UtilityFunc::getRamUsageByPID(int &pid)
{
    float value = 0;
    std::string cmd = "ps -p " + std::to_string(pid) + " -o %ram | tail -1 | awk '{print $1}'";
    std::string output = UtilityFunc::exeCommand(cmd);
    if (output.empty() == false)
    {
        value = std::stof(output);
    }

    return value / 1000;
}

int UtilityFunc::getPortListenByPID(const int &pid, const int &index)
{
    int port = 0;
    std::string cmd = "netstat -tunlp | grep " + std::to_string(pid) + " | awk 'NR == " + std::to_string(index) +" { print $4 }'";
    std::string output = UtilityFunc::exeCommand(cmd);
    if (output.empty() == false)
    {
        // split port
        std::string str_spl[10];
        UtilityFunc::splitString(output, ":", str_spl);
        if(str_spl[1].empty() == false){
            port = std::stoi(str_spl[1]);
        }
    }

    return port;
}

long UtilityFunc::getAvailableSpace(const std::string &path)
{
    std::string cmd = "df " + path + " | awk 'NR == 2 { print $4 }'";
    std::string get_out = UtilityFunc::exeCommand(cmd.c_str());
    if (!get_out.empty())
    {
        return (std::stol(get_out));
    }
    return 0;
}

long UtilityFunc::getTotalSpace(const std::string &path)
{
    std::string cmd = "df " + path + " | awk 'NR == 2 { print $2 }'";
    std::string get_out = UtilityFunc::exeCommand(cmd.c_str());
    if (!get_out.empty())
    {
        return (std::stol(get_out));
    }
    return 0;
}

bool UtilityFunc::getLocalIpAddr(std::string &ip_addr){
    std::string cmd = "hostname -I | awk '{print $1}'";
    std::string get_out = UtilityFunc::exeCommand(cmd.c_str());
    if (!get_out.empty())
    {
        get_out.erase(std::remove(get_out.begin(), get_out.end(), '\n'), get_out.end());
        ip_addr = get_out;
        return true;
    }
    return false;
}