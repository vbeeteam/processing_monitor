/**
******************************************************************************
* @file           : utility.hpp
* @brief          : header file of utility function
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef UTILITY_FUNCTION_H
#define UTILITY_FUNCTION_H

#include <stdint.h>
#include <stdbool.h>
#include <string>
#include <fstream>
#include <iostream>
#include <math.h>
#include <iomanip>
#include <chrono>
#include <vector>
#include <sstream>

namespace CommonEngine
{

    const int NUM_CPU_STATES = 10;

    enum CPUStates
    {
        S_USER = 0,
        S_NICE,
        S_SYSTEM,
        S_IDLE,
        S_IOWAIT,
        S_IRQ,
        S_SOFTIRQ,
        S_STEAL,
        S_GUEST,
        S_GUEST_NICE
    };

    typedef struct CPUData
    {
        std::string cpu;
        size_t times[NUM_CPU_STATES];
    } CPUData;

    class CpuMonitor
    {
    private:
    public:
        CpuMonitor();
        ~CpuMonitor();

        void ReadStatsCPU(std::vector<CPUData> &entries);

        size_t GetIdleTime(const CPUData &e);
        size_t GetActiveTime(const CPUData &e);
        void PrintStats(const std::vector<CPUData> &entries1, const std::vector<CPUData> &entries2);
    };

    typedef struct _fields_url_info
    {
        std::string scheme;
        std::string host;
        int port;
        std::string path;
    } fields_url_info;

    class UtilityFunc
    {
    private:
    public:
        UtilityFunc()
        {
        }
        ~UtilityFunc()
        {
        }

        static uint32_t getTimestamp(void);
        static uint32_t getTimeMs(void);
        static uint8_t splitString(std::string data, std::string key_split, std::string *data_split);
        static bool checkFile(std::string path);
        static std::string fileName(std::string path);
        static bool creatFile(std::string path);
        static uint16_t getCurrentAsMinuteInDay(void);
        static bool parseUrl(const std::string &url, fields_url_info &urlInfo);

        static std::string exeCommand(const std::string &cmd);
        static bool getPIDByProcess(const std::string &p_name, int &pid);
        static uint16_t getCpuUsage(void);
        static uint16_t getRamUsage(void);
        static float getCpuUsageByPID(int &pid);
        static float getRamUsageByPID(int &pid);
        static int getPortListenByPID(const int &pid, const int &index);

        static long getAvailableSpace(const std::string &path);
        static long getTotalSpace(const std::string &path);
        static bool getLocalIpAddr(std::string &ip_addr);
    };
}

#endif // !UTILITY_FUNCTION_H
