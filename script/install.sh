sudo apt install libcurl4-nss-dev -y
rm -r build
mkdir build && cd build
cmake -DCMAKE_INSTALL_PREFIX=/opt/pm_app/ ..
make -j4
make install
echo "Enable auto start service"
pwd
sudo cp ../script/pm_app.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl stop pm_app.service
sudo systemctl enable pm_app.service
sudo systemctl restart pm_app.service