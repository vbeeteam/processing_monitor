/**
******************************************************************************
* @file           : app_monitor.cpp
* @brief          : Source file of application monitor
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#define CPPHTTPLIB_OPENSSL_SUPPORT
#include "http_lib/httplib.h"
#include "app_monitor.hpp"
#include <iostream>
#include <unistd.h>
#include "utility/utility.hpp"
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include <thread>

using namespace CommonEngine;
using namespace httplib;
using namespace rapidjson;

int AppMonitor::monitor_task(void)
{
    std::cout << "start monitor task" << std::endl;

    while (1)
    {
        // get pid by process name
        for (uint16_t i = 0; i < this->m_app_info.process_name.size(); i++)
        {
            std::string process_name = this->m_app_info.process_name[i];
            int pid;
            // add new process
            if (this->p_monitor.find(process_name) == this->p_monitor.end())
            {
                ProcessInfo process_info;
                process_info.pid = pid;
                process_info.name = process_name;
                this->p_monitor[process_name] = process_info;
                std::cout << "Add new process = " << process_name << " - PID = " << std::to_string(pid) << std::endl;
            }
        }

        // monitor all process
        std::cout << "CHEKING ALL PROCESS AS PID..." << std::endl;
        std::map<std::string, ProcessInfo>::iterator map;
        for (map = this->p_monitor.begin(); map != this->p_monitor.end(); map++)
        {
            ProcessInfo process_info = map->second;
            // check state
            int pid = 0;
            process_info.is_running = false;
            if (UtilityFunc::getPIDByProcess(process_info.name, pid))
            {
                process_info.is_running = true;
                process_info.pid = pid;
            }
            if (process_info.is_running)
            {
                // get info
                process_info.cpu = UtilityFunc::getCpuUsageByPID(process_info.pid);
                process_info.ram = UtilityFunc::getRamUsageByPID(process_info.pid);
                // port
                uint16_t max_port = 500;
                process_info.ports_listen.clear();
                for (uint16_t i = 1; i < max_port; i++)
                {
                    int port_listen = UtilityFunc::getPortListenByPID(process_info.pid, i);
                    if (port_listen == 0)
                    {
                        break;
                    }
                    process_info.ports_listen.push_back(port_listen);
                }
            }
            // update to list
            this->p_monitor[process_info.name] = process_info;
        }

        // monitor common process
        sleep(10);
    }
}

int AppMonitor::report_task(void)
{
    std::cout << "start report task" << std::endl;
    sleep(2);
    while (1)
    {
        sleep(1);
        try
        { 
            std::cout << "start report" << std::endl;
            // get common monitor
            int cpu = UtilityFunc::getCpuUsage();
            int ram = UtilityFunc::getRamUsage();
            long free_disk = UtilityFunc::getAvailableSpace(this->m_app_info.disk_path);
            long total_disk = UtilityFunc::getTotalSpace(this->m_app_info.disk_path);
            std::string local_ip_addr;
            if (UtilityFunc::getLocalIpAddr(local_ip_addr) == false)
            {
                local_ip_addr = "localhost";
            }

            // attach process monitor
            StringBuffer pm_string_buffer;
            Writer<StringBuffer> pm_writer(pm_string_buffer);

            // pm_writer.StartArray();
            std::map<std::string, ProcessInfo>::iterator map;
            bool is_check_process = true;
            std::string error_msg = "error process: ";
            for (map = this->p_monitor.begin(); map != this->p_monitor.end(); map++)
            {
                ProcessInfo process_info = map->second;
                if (process_info.is_running == false)
                {
                    is_check_process = false;
                    error_msg += process_info.name + ",";
                }

                pm_writer.StartObject();
                pm_writer.Key("pid");
                pm_writer.Int64(process_info.pid);
                pm_writer.Key("name");
                pm_writer.String(process_info.name.c_str());
                pm_writer.Key("cpu");
                pm_writer.Int((int)process_info.cpu);
                pm_writer.Key("memory");
                pm_writer.Int((int)process_info.ram);
                pm_writer.Key("Ports");
                pm_writer.StartArray();
                for (uint16_t i = 0; i < process_info.ports_listen.size(); i++)
                {
                    pm_writer.Int64(process_info.ports_listen[i]);
                }
                pm_writer.EndArray();
                pm_writer.EndObject();
                break;
            }

            // pm_writer.EndArray();

            std::string process_data = pm_string_buffer.GetString();
            // if (is_check_process == false)
            // {
            //     process_data = error_msg;
            //     if(this->is_report_error == true){
            //         continue;
            //     }
            //     this->is_report_error = true;
            // }
            // else{
            //     this->is_report_error = false;
            // }

            StringBuffer string_buff;
            Writer<StringBuffer> writer(string_buff);

            writer.StartObject();
            writer.Key("cpu");
            writer.Uint(cpu);
            writer.Key("memory");
            writer.Uint(ram);
            writer.Key("freeDisk");
            writer.String(std::to_string(free_disk).c_str());
            writer.Key("totalDisk");
            writer.String(std::to_string(total_disk).c_str());
            writer.Key("localIp");
            writer.String(local_ip_addr.c_str());
            writer.Key("host");
            writer.String(this->m_app_info.ipAddress.c_str());
            writer.Key("application");
            writer.String(this->m_app_info.application.c_str());
            writer.Key("infoAttachment");
            writer.String(process_data.c_str());

            writer.EndObject();

            std::string data_report = string_buff.GetString();

            // API report
            std::cout << "report data = " << data_report << " - api = " << this->m_app_info.report_api << std::endl;
            this->report_api(this->m_app_info.report_api, this->m_app_info.api_token, data_report);
        }
        catch (const std::exception &e)
        {
            std::cerr << e.what() << '\n';
        }
        sleep(this->m_app_info.report_period);
    }
}

bool AppMonitor::report_api(std::string &api, std::string &token, std::string &data)
{
    // std::string api_url = "https://resource-api.beex.vn:8050/resourceClouds/ping";
    // std::string token = "Basic NS01BhqBYSksuvhqJO0b16O86C0wPwkM";
    std::string respone;

    // parse url
    fields_url_info url_info;
    if (UtilityFunc::parseUrl(api, url_info) == false)
    {
        return false;
    }

    std::string body = "";
    if (url_info.scheme == "https")
    {
        // note cacert file
        httplib::SSLClient cli(url_info.host, url_info.port);
        // cli.set_ca_cert_path ( "./ca-bundle.crt" );
        cli.enable_server_certificate_verification(false);
        // use header
        httplib::Headers headers = {
            {"Authorization", token}};
        // std::cout << "host = " << url_info.host << " - port = " << std::to_string(url_info.port) << " - path  = " << url_info.path << std::endl;
        auto res = cli.Post(url_info.path.c_str(), headers, data.c_str(), "application/json");
        auto err = res.error();
        std::cout << "report_api : res->status = " << res->status << " - error = " << err << std::endl;
        std::cout << "report_api:  res->body   = " << res->body << std::endl;

        if (err == httplib::Error::Success)
        {
            if (res->status == 200)
            {
                body = res->body;
            }
        }
    }
    else if (url_info.scheme == "http")
    {
        httplib::Client cli(url_info.host, url_info.port);
        httplib::Headers headers = {
            {"Authorization", token}};
        auto res = cli.Post(url_info.path.c_str(), headers, data.c_str(), "application/json");
        auto err = res.error();
        if (err == httplib::Error::Success)
        {
            if (res->status == 200)
            {
                body = res->body;
            }
        }
    }
    return true;
}

int AppMonitor::run(void){
    std::cout << "start monitor for application = " << this->m_app_info.application << std::endl;
    
    std::thread MonitorTask(&AppMonitor::monitor_task, this);
    std::thread ReportTask(&AppMonitor::report_task, this);

    MonitorTask.join();
    ReportTask.join();

    std::cout << "stop monitor for application = " << this->m_app_info.application << std::endl;
}