/**
******************************************************************************
* @file           : config.cpp
* @brief          : source file of configuration
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#include "config.hpp"
#include "utility/utility.hpp"
#include "rapidjson/rapidjson.h"
#include "rapidjson/document.h"

using namespace CommonEngine;
using namespace rapidjson;

bool ConfigHandler::loadConfig(void){
    bool result = false;
    std::cout << "load config = " << this->config_file << std::endl;
    if(UtilityFunc::checkFile(this->config_file)){
        FILE *file = NULL;
        size_t len;
        ssize_t read;
        char *line = NULL;
        file = fopen(this->config_file.c_str(), "r");
        if (file != NULL)
        {
            fseek(file, 0, SEEK_END);
            long fsize = ftell(file);
            fseek(file, 0, SEEK_SET); /* same as rewind(f); */
            char *string = (char *)malloc(fsize + 1);
            fread(string, 1, fsize, file);
            std::string json_data(string);
            std::cout << json_data << std::endl;
            try
            {
                // parse json data
                Document document;
                document.Parse(json_data.c_str());
                for (uint16_t i = 0; i < document.Size(); i++){
                    const Value &app_obj = document[i];
                    ConfigInfo config_info;
                    if (app_obj.HasMember("application"))
                    {
                        std::string application = app_obj["application"].GetString();
                        config_info.application = application;
                    }
                    if (app_obj.HasMember("reportAPI"))
                    {
                        std::string report_api = app_obj["reportAPI"].GetString();
                        config_info.report_api = report_api;
                    }
                    if (app_obj.HasMember("apiToken"))
                    {
                        std::string api_token = app_obj["apiToken"].GetString();
                        config_info.api_token = api_token;
                    }
                    if (app_obj.HasMember("diskPath"))
                    {
                        std::string disk_path = app_obj["diskPath"].GetString();
                        config_info.disk_path = disk_path;
                    }
                    if (app_obj.HasMember("processName"))
                    {
                        const Value &process_name = app_obj["processName"];
                        if (process_name.IsArray())
                        {
                            for (uint16_t i = 0; i < process_name.Size(); i++)
                            {
                                std::string process = process_name[i].GetString();
                                config_info.process_name.push_back(process);
                                std::cout << "debug = " << process << std::endl;
                            }
                        }
                    }
                    if (app_obj.HasMember("reportPeriod"))
                    {
                        int report_period = app_obj["reportPeriod"].GetInt();
                        config_info.report_period = report_period;
                    }
                    if (app_obj.HasMember("ipAddress"))
                    {
                        std::string ipAddress = app_obj["ipAddress"].GetString();
                        config_info.ipAddress = ipAddress;
                    }
                    this->configs[config_info.application] = config_info;

                    result = true;
                }
            }
            catch (const std::exception &e)
            {
                std::cout << "Config::loadConfig - exception when parse json data = " << json_data << std::endl;
                result = false;
            }
            fclose(file);
            free(string);
        }
        else
        {
            std::cout << "Config::loadConfig - config file not opened" << std::endl;
        }
    }
    else{
        std::cout << "config file not found" << std::endl;
        return false;
    }

    return result;
}